<?php

namespace Enmaboya\TextSoc;

use DB;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class TextSoc extends Field
{
    public $relClass, $changeField;
    public $component = 'text-soc';

    public function relateClass($relclass){
        $this->relClass = $relclass;
        return $this->withMeta([
            'relateClass' => $relclass
        ]);
    }

    public function field($changeField){
        $this->changeField = $changeField;
        return $this->withMeta([
            'field' => $changeField
        ]);
    }
 
    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {   
        $check = DB::table($model->getTable())->where('id', $model->id)->first();
        $field = $this->changeField;
        if (is_null($request[$requestAttribute])) {
            if ($check <> null) {
                DB::table($model->getTable())
                        ->where($this->changeField, $model->$field)
                        ->update([$this->changeField => null]);
            }
            return;
        }
        $class = get_class($model);
        $class::saved(function ($model) use ($requestAttribute, $request, $field) {
            $element = $this->relClass::firstOrCreate(
                ['title' => $request[$requestAttribute]], ['city_id' => $model->city_id]
            );
            DB::table($model->getTable())
                        ->where('id', $model->id)
                        ->update([$this->changeField => $element->id]);
        });
    }
}
