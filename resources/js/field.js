Nova.booting((Vue, router) => {
    Vue.component('index-text-soc', require('./components/IndexField'));
    Vue.component('detail-text-soc', require('./components/DetailField'));
    Vue.component('form-text-soc', require('./components/FormField'));
})
